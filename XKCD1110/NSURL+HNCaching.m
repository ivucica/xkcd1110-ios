//
//  NSURL+HNCaching.m
//  HN
//
//  Created by Ivan Vučica on 3.7.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import "NSURL+HNCaching.h"

@implementation NSURL (HNCaching)

-(NSString*)cacheFile
{
    NSString * cacheFile = [[[[self absoluteString] stringByDeletingLastPathComponent] lastPathComponent] stringByAppendingString: [[self absoluteString] lastPathComponent]];
    NSString * cachePath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    return [cachePath stringByAppendingPathComponent:cacheFile];
}

#if 1
#define CACHE_TIME 30*60 // 30min
#else
#define CACHE_TIME 5 // 5 sec
#warning Cache time set to 5 sec!
#endif
- (BOOL)cacheExists
{
    return [[NSFileManager defaultManager] fileExistsAtPath:[self cacheFile]];
}
- (BOOL)cacheValid
{
    return [self cacheExists] && 
    [[[[NSFileManager defaultManager] attributesOfItemAtPath:[self cacheFile] error:nil] valueForKey:NSFileModificationDate] timeIntervalSinceNow] > -CACHE_TIME;
}

- (void)cacheDestroy
{
    [[NSFileManager defaultManager] removeItemAtPath:[self cacheFile] error:nil];
}
@end
