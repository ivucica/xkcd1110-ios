//
//  IVMainViewController.h
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "IVFlipsideViewController.h"

@class IVXkcdView;

@interface IVMainViewController : UIViewController <IVFlipsideViewControllerDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) UIPopoverController *flipsidePopoverController;

- (IBAction)showInfo:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *statusLabel;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet IVXkcdView *xkcdView;

@end
