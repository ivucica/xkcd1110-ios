//
//  UIImage+HNCaching.h
//  HN
//
//  Created by Ivan Vučica on 10.7.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (HNCaching)
+ (UIImage*) imageWithContentsOfURLAndCaching: (NSURL*)url;
@end
