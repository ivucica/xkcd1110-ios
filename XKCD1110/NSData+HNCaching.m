//
//  NSData+HNCaching.m
//  HN
//
//  Created by Ivan Vučica on 24.8.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import "NSData+HNCaching.h"
#import "NSURL+HNCaching.h"
//#import "ActivityManager.h"

@implementation NSData (HNCaching)
+ (NSData*) dataWithContentsOfURLAndCaching: (NSURL*)url
{
    NSData * data;
    
    if(![url cacheValid])
    {
        //[[ActivityManager sharedManager] addActivity];
        data = [NSData dataWithContentsOfURL: url];
        //[[ActivityManager sharedManager] removeActivity];
        if(data)
        {
            [data writeToFile: [url cacheFile] atomically: YES];
            return data;
        }
    }
    
    return [self dataWithContentsOfFile: [url cacheFile]];
}


@end
