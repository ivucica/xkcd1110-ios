//
//  UIImage+HNCaching.m
//  HN
//
//  Created by Ivan Vučica on 10.7.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import "UIImage+HNCaching.h"
//#import "ActivityManager.h"
#import "NSURL+HNCaching.h"

@implementation UIImage (HNCaching)
+ (UIImage*) imageWithContentsOfURLAndCaching: (NSURL*)url
{
    
    NSData * data;
    
    if(![url cacheValid])
    {
        //[[ActivityManager sharedManager] addActivity];
        data = [NSData dataWithContentsOfURL: url];
        //[[ActivityManager sharedManager] removeActivity];
        if(data)
        {
            UIImage * img = [self imageWithData:data];
            if(img)
            {
                [data writeToFile: [url cacheFile] atomically: YES];
                return img;
            }
        }
    }
    
    return [self imageWithContentsOfFile: [url cacheFile]];
}

@end
