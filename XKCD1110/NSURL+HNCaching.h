//
//  NSURL+HNCaching.h
//  HN
//
//  Created by Ivan Vučica on 3.7.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (HNCaching)
-(NSString*)cacheFile;
-(BOOL)cacheExists;
-(BOOL)cacheValid;
-(void)cacheDestroy;
@end
