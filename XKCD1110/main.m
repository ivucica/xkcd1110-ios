//
//  main.m
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IVAppDelegate class]));
    }
}
