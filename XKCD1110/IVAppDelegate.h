//
//  IVAppDelegate.h
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IVMainViewController;

@interface IVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) IVMainViewController *mainViewController;

@end
