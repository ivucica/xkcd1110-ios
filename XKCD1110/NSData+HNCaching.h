//
//  NSData+HNCaching.h
//  HN
//
//  Created by Ivan Vučica on 24.8.2012..
//  Copyright (c) 2012. Hindarium. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (HNCaching)
+ (NSData*) dataWithContentsOfURLAndCaching: (NSURL*)url;
@end
