//
//  IVXkcdView.m
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "IVXkcdView.h"
#import <QuartzCore/QuartzCore.h>
#import "IVXkcdTiledLayer.h"

@implementation IVXkcdView

+ (Class)layerClass
{
    return [IVXkcdTiledLayer class];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CATiledLayer *tempTiledLayer = (CATiledLayer*)self.layer;

        tempTiledLayer.levelsOfDetail = 10;

        tempTiledLayer.levelsOfDetailBias = 9;


    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(!self)
        return nil;
    CATiledLayer *tempTiledLayer = (CATiledLayer*)self.layer;
    
    tempTiledLayer.levelsOfDetail = 10;
    
    tempTiledLayer.levelsOfDetailBias = 9;
    

    return self;
}
/*
- (void)didMoveToSuperview
{
 // superview.maximumZoomScale returns 0 even here, despite superview being a scrollview :/
 
    CATiledLayer *tempTiledLayer = (CATiledLayer*)self.layer;
    
    tempTiledLayer.levelsOfDetail = ((UIScrollView*)self.superview).maximumZoomScale;
    
    tempTiledLayer.levelsOfDetailBias = ((UIScrollView*)self.superview).maximumZoomScale-1;
    
    NSLog(@"LOD in superview %@: %d", self.superview, tempTiledLayer.levelsOfDetail);
    
    [tempTiledLayer setNeedsDisplay];

}
 */
/*
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    CGContextRef context = UIGraphicsGetCurrentContext();

}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    NSLog(@"Touchesbegan %d", touches.count);
}
@end
