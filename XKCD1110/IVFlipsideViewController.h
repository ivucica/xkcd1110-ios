//
//  IVFlipsideViewController.h
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IVFlipsideViewController;

@protocol IVFlipsideViewControllerDelegate
- (void)flipsideViewControllerDidFinish:(IVFlipsideViewController *)controller;
@end

@interface IVFlipsideViewController : UIViewController

@property (assign, nonatomic) id <IVFlipsideViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;

@end
