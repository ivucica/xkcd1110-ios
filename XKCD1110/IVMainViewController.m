//
//  IVMainViewController.m
//  XKCD1110
//
//  Created by Ivan Vučica on 29.9.2012..
//  Copyright (c) 2012. Ivan Vučica. All rights reserved.
//

#import "IVMainViewController.h"
#import "IVXkcdView.h"

@interface IVMainViewController ()

@end

@implementation IVMainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.scrollView zoomToRect:CGRectMake(0, 0, 256, 256) animated:NO];
    [self performSelector:@selector(initialZoom) withObject:nil afterDelay:0.5];
    self.title = @"xkcd #1110";
}

- (void)initialZoom
{
    [UIView beginAnimations:@"initialZoom" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:5];
    [self.scrollView zoomToRect:CGRectFromString(@"{{127.928, 127.082}, {0.498523, 0.623154}}") animated:NO];
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Flipside View Controller

- (void)flipsideViewControllerDidFinish:(IVFlipsideViewController *)controller
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.flipsidePopoverController dismissPopoverAnimated:YES];
    }
}

- (void)dealloc
{
    [_flipsidePopoverController release];
    [_xkcdView release];
    [_scrollView release];
    [_statusLabel release];
    [super dealloc];
}

- (IBAction)showInfo:(id)sender
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        IVFlipsideViewController *controller = [[[IVFlipsideViewController alloc] initWithNibName:@"IVFlipsideViewController" bundle:nil] autorelease];
        controller.delegate = self;
        controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        if (!self.flipsidePopoverController) {
            IVFlipsideViewController *controller = [[[IVFlipsideViewController alloc] initWithNibName:@"IVFlipsideViewController" bundle:nil] autorelease];
            controller.delegate = self;
            
            self.flipsidePopoverController = [[[UIPopoverController alloc] initWithContentViewController:controller] autorelease];
        }
        if ([self.flipsidePopoverController isPopoverVisible]) {
            [self.flipsidePopoverController dismissPopoverAnimated:YES];
        } else {
            [self.flipsidePopoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
    }
}

#pragma mark - Scrollview delegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.xkcdView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateStatus];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self updateStatus];
}

- (void)updateStatus
{
    NSString * status = [NSString stringWithFormat:@"Scale: %g", self.scrollView.zoomScale];
    self.statusLabel.text = status;
    
    CGRect visibleRect = {0};
    
    visibleRect.origin = self.scrollView.contentOffset;
    visibleRect.origin.x /= self.scrollView.zoomScale;
    visibleRect.origin.y /= self.scrollView.zoomScale;
    
    visibleRect.size = self.scrollView.bounds.size;
    visibleRect.size.width /= self.scrollView.zoomScale;
    visibleRect.size.height /= self.scrollView.zoomScale;
    
    //NSLog(@"Visible %@", NSStringFromCGRect(visibleRect));
    
}
@end
