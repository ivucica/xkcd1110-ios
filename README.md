xkcd #1110 for iOS
==================

Inspired by recent discussions on how long it could take Google to develop
standalone Google Maps, I decided to prove my opinion that it should take very,
very short time.

So based on the idea from Florian Wesch - @dividuum - and using his tiles for
Randall Munroe's xkcd comic #1110):
  http://xkcd-map.rent-a-geek.de
I've decided to develop a small app that would display just the "map" data.

It took me around 3h.

How long would it take Google to add basic search and routing to such a maps
app? I estimate it should take them at most, in the worst case scenario, about
3 months to be at the same stage as the pre-iOS 6 Maps app.

Again, that's the worst case scenario, with Google's resources for hiring
engineers.

This app uses `CATiledLayer` and the fact that its `-drawInContext:` is
multithreaded. This means we can even download the tiles during
`-drawInContext:`. To be nicer to Florian's and user's bandwidth, there's also
some caching of the downloaded images.

View which contains the `CATiledLayer` subclass doesn't do anything except
contain this special layer. `UIScrollView` does all the hard work of handling
scrolling and zooming, which `CATiledLayer` happily picks up and orders drawing
of its own contents.

This is a universal app for iPhone and iPad. It was tested only in the
simulator, and also seems to work fine on simulated iPhone 5.

-- Ivan Vučica <ivan@vucica.net>

